@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h1>Add Event</h1></div>

                    <div class="card-body">
                        @if(session()->has('message'))
                            <p class="alert-info">
                                {{session()->get('message')}}
                            </p>
                        @endif
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="/admin/event" method="POST">
                                    @csrf
                                    <p>Event Title :</p>
                                    <input type="text" style="width: 50%;margin-bottom: 30px" name="title" required>
                                    <p>Event Date :</p>
                                    <input type="Date" style="margin-bottom: 30px" name="date" required>
                                    <p>Event Start Time :</p>
                                    <input type="time" style="margin-bottom: 30px" name="STime" required>
                                    <p>Event End Time :</p>
                                    <input type="time" style="margin-bottom: 30px" name="ETime" required>
                                    <br/>
                                    <button class="btn btn-primary" style="padding-left: 20px;padding-right: 20px;background-color: #4BB543;border-color: #4BB543" type="submit">Add Event</button>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
