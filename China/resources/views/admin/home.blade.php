@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h1>Pending Member</h1></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <td>ID</td>
                                            <td>Name</td>
                                            <td>Email</td>
                                            <td>Status</td>
                                            <td colspan = 2>Actions</td>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($admin as $contact)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$contact->name}}</td>
                                                <td>{{$contact->email}}</td>
                                                <td><mark style="background-color: grey;color: white;text-align: center;font-size: 12px">PENDING</mark></td>
                                                <td>
                                                    <form method="post" action="{{ route('admin.admin.update', $contact->id) }}">
                                                        @csrf
                                                        @method('PUT')
                                                        <button style="background-color: transparent;border-color: transparent;padding: 0px" type="submit"  name="verify" value="1"><i  style="color: green;font-size: 30px" class="fa fa-check" aria-hidden="true"></i>
                                                        </button>
                                                        <span style="padding-left: 20px;padding-right: 20px"></span>
                                                        <button style="background-color: transparent;border-color: transparent;padding: 0px" type="submit" name="verify" value="2"><i style="color: #ff0000;font-size: 30px" class="fa fa-trash" aria-hidden="true"></i></button>
                                                    </form>
                                                </td>


                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div>
                                    </div>

                                </div>
                            </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top: 50px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h1>Member</h1></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>Name</td>
                                        <td>Email</td>
                                        <td>Status</td>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($approve as $contact)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$contact->name}}</td>
                                            <td>{{$contact->email}}</td>
                                            <td><mark style="background-color: #4BB543;color: white;text-align: center;font-size: 12px">APPROVE</mark></td>


                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="container"  style="margin-top: 50px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h1>Fans</h1></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>Name</td>
                                        <td>Email</td>
                                        <td>Status</td>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($reject as $contact)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$contact->name}}</td>
                                            <td>{{$contact->email}}</td>
                                            <td><mark style="background-color: red;color: white;text-align: center;font-size: 12px">REJECTED</mark></td>


                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
