@extends('layouts.user')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h1>Registered Event</h1></div>

                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
{{--                                <td>ID</td>--}}
                                <td>Event</td>
                                <td>Date</td>
                                <td>Time</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($event as $events)
                                <tr>
{{--                                    <td>{{$loop->iteration}}</td>--}}
                                    <td>{{$events->title}}</td>
                                    <td>{{$events->date}}</td>
                                    <td>{{$events->start_time}} ~ {{$events->end_time}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
@endsection
