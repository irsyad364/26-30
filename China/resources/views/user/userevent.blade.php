@extends('layouts.user')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h1>List Event</h1></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>Event</td>
                                        <td>Date</td>
                                        <td>Time</td>
                                        <td colspan = 2>Actions</td>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($event as $events)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                        @foreach($users as $user)
                                            <td>{{$events->title}}</td>
                                            <td>{{$events->date}}</td>
                                            <td>{{$events->start_time}} ~ {{$events->end_time}}</td>
                                            <td>
                                                <form method="post" action="{{ route('user.user.update', $user->id) }}">
                                                    @csrf
                                                    @method('PUT')
                                                    <button class="btn btn-primary" style="background-color: #4BB543" type="submit"  name="event" value="{{$events->event_id}}">Join
                                                    </button>
                                                </form>
                                            </td>


                                        </tr>
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                                <div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
