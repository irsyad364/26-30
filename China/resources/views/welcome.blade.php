<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>China Olympics</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                background-image:linear-gradient(rgba(0,0,0,0.7),rgba(0,0,0,0.7)), url(https://www.workandliveinchina.com/wp-content/uploads/2019/06/Beautiful-China-Great-Wall.jpg);
                height: 100vh;
                background-size: cover;
                background-position:center;
            }
            hr.rounded {
                border-top: 5px solid white;
                border-radius: 5px;
                width: 90%;
            }
            table {
                color: #588c7e;
                font-family: monospace;
                font-size: 20px;
                text-align: left;
                margin-left: auto;
                margin-right: auto;
                margin-bottom: 50px;
            }
            th {
                text-align: center;
                padding-top: 10px;
                padding-bottom: 10px;
                background-color: #c7ba02;
                color: white;
            }
            td {
                padding-top: 20px;
                padding-bottom: 20px;
                text-align: center;
                color: black;
                background-color: #dbd79e
            }
        </style>
    </head>
    <body class="antialiased">
            @if (Route::has('login'))
                <div style="background-color: #241818;width: 100%;position: sticky;height: 11%">
                    <div style="width: 70%;margin-left: 300px;padding-top: 20px">
                    <div style="float: left">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Chinese_Olympic_Committee_logo.svg/1200px-Chinese_Olympic_Committee_logo.svg.png" alt="Lamp" width="80" height="70">
                        </a>
                    </div>
                    <div style="position: absolute;left: 48%;width: 20%">
                        <p style="font-size: 20px;color: white">Republic Of China</p>
                    </div>
                    <div style="float: right;padding-top: 20px">
                    @auth
                        @if (auth()->user()->is_admin)
                            <a href="{{ url('/admin') }}" class="text-sm text-gray-700" style="color: #9e9e9e">Home</a>
                            @else
                                <a href="{{ url('/user') }}" class="text-sm text-gray-700" style="color: #9e9e9e">Home</a>
                        @endif

                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700" style="color: #9e9e9e">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700" style="color: #9e9e9e">Register</a>
                        @endif
                    @endauth
                    </div>
                    </div>
                </div>
            @endif

            <div class="main" style="background-color: black;width: 96%;position: relative;top: 2%;left: 2%;padding-bottom: 100px;border: 3px solid #f1f1f1;opacity: 0.8">
                <div style="padding-bottom: 200px">
                    <p style="text-align: center;font-size: 40px;padding-top: 20px;color: white"><b><u>China</u></b></p>
                    <div style="text-align: center">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People%27s_Republic_of_China.svg/1200px-Flag_of_the_People%27s_Republic_of_China.svg.png" alt="Lamp" width="500" height="300">
                    </div>


                    <div class="text" style="padding-top: 100px;padding-left: 400px;padding-right: 400px">
                        <p style="font-size: 30px;color: white"><b>1) Introduction</b></p>
                        <div class="text" style="padding-left: 100px;padding-right: 100px;padding-bottom: 100px">
                            <p style="font-size: 20px;color: white;text-align: justify">China, officially the People's Republic of China (PRC), is a country in East Asia. It is the world's most populous country, with a population of around 1.4 billion.[13] China covers an area of approximately 9.6 million square kilometers (3.7 million mi2), it is the world's third or fourth-largest country.[k] The country is officially divided into 23 provinces,[l][19] five autonomous regions, four direct-controlled municipalities (Beijing, Tianjin, Shanghai, and Chongqing), and two special administrative regions of Hong Kong and Macau.</p>
                        </div>
                        <p style="font-size: 30px;color: white"><b>2) Culturer</b></p>
                        <div class="text" style="padding-left: 100px;padding-right: 100px">
                            <p style="font-size: 20px;color: white;text-align: justify">Chinese culture is one of the world’s oldest cultures, tracing back to thousands of years ago. Important components of Chinese culture includes ceramics, architecture, music, literature, martial arts, cuisine, visual arts, philosophy and religion.</p>
                        </div>
                        <div class="text" style="padding-left: 100px;padding-right: 100px">
                            <p style="font-size: 30px;color: white;text-align: justify;padding-top: 50px"><b>2.1) Ethnic Groups</b></p>
                            <div class="text" style="padding-left: 100px;padding-right: 100px">
                                <div style="text-align: center">
                                    <img src="https://stunningtours.com/wp-content/uploads/2019/11/Miao-girl_no-caption%E6%94%B9-768x523.jpg" alt="Lamp" width="500" height="300">
                                </div>
                                <p style="font-size: 20px;color: white;text-align: justify">Officially there are 56 recognized ethnic groups in China, Han Chinese being the largest group. Many ethnic groups, though merge into Han identity, have maintained distinct linguistic and regional cultural traditions. Even within one ethnic group, there are probably diverse groups of people. Various groups of the Miao minority, for example, speak different dialects of the Hmong-Mie languages, Tai-Kadai languages, and Chinese, and practice a variety of different cultural customs. Typically each minority group has their own costumes, festivals and customs. For example, various marriage customs  are found among different minority groups. There is actually a museum that features marriage customs of ethnic groups-Guizhou Museum of Marriage Customs.</p>
                            </div>
                            <p style="font-size: 30px;color: white;text-align: justify;padding-top: 50px"><b>2.2) Religion</b></p>
                            <div class="text" style="padding-left: 100px;padding-right: 100px">
                                <p style="font-size: 20px;color: white;text-align: justify">Confucianism and Taoism, later joined by Buddhism, constitute the “three teachings” that historically have shaped Chinese culture. There are no clear boundaries between these intertwined religious systems, which don’t claim to be exclusive, and elements of each enrich popular or folk religion. Folk or popular religion, the most widespread system of beliefs and practices has evolved and adapted since at least Shang and Zhou dynasties. During the period fundamental elements of a theology and spiritual explanation for the nature of the universe emerged. Basically, it consists in allegiance to the “shen”, a character that signifies a variety of gods and immortals, who can be deities of the natural environment of ancestral principles of human groups, concepts of civility, cultural heroes, many of whom feature in Chinese mythology and history. Recent surveys estimated that some 80% of Han Chinese practice some kind of Chinese folk religion and Taoism; 10-16% are Buddhists; 3-4% are Christians; and 1-2% are Muslims.</p>
                            </div>
                            <p style="font-size: 30px;color: white;text-align: justify;padding-top: 50px"><b>2.3) Chinese Medicine</b></p>
                            <div class="text" style="padding-left: 100px;padding-right: 100px">
                                <div style="text-align: center">
                                    <img src="https://stunningtours.com/wp-content/uploads/2019/11/herbal-medicine-w-caption.jpg" alt="Lamp" width="500" height="300">
                                </div>
                                <p style="font-size: 20px;color: white;text-align: justify">Traditional Chinese medicine is built on a foundation of more than 2,500 years of Chinese medical practices that includes various forms of herbal medicine, acupuncture, massage, exercise and dietary therapy.  Its philosophy is based on Yinyangism,(the combination of Five Phases theory with Yin-Yang theory) which was later absorbed by Daoism. In general, disease is perceived as a disharmony or imbalance in the functions or interactions of yin, yang, meridians etc. between the human body and the environment. Therapy is based on which “pattern of disharmony” can be identified. Today Traditional Chinese medicine is widely used in China and is becoming increasingly prevalent in Europe and North America.</p>
                            </div>
                            <p style="font-size: 30px;color: white;text-align: justify;padding-top: 50px"><b>2.4) Chinese Family</b></p>
                            <div class="text" style="padding-left: 100px;padding-right: 100px">
                                <div style="text-align: center">
                                    <img src="https://stunningtours.com/wp-content/uploads/2019/11/chinese-family-%E6%94%B9.jpg" alt="Lamp" width="500" height="300">
                                </div>
                                <p style="font-size: 20px;color: white;text-align: justify">Family has been a key component in society for thousands of years in China. Today, many aspects of Chinese life can be tied to honoring one’s parents or ancestors. Because of the focus on family, it is common for Chinese, even when fully grown with children of their own, to have many living generations of a family living under the same roof.</p>
                            </div>
                            <p style="font-size: 30px;color: white;text-align: justify;padding-top: 50px"><b>2.5) Chinese Food</b></p>
                            <div class="text" style="padding-left: 100px;padding-right: 100px">
                                <p style="font-size: 20px;color: white;text-align: justify">The “Eight Cuisines” of China are Anhui, Cantonese, Fujian, Hunan, Jiangsu, Shandong, Sichuan, and Zhejiang cuisines. These styles are distinctive from one another due to factors such as availability of resources, climate, geography, history, cooking techniques and lifestyle. For example, Jiangsu cuisine favours cooking techniques such as braising and stewing, while Sichuan cuisine employs baking. Hairy crab is a highly sought-after local delicacy in Shanghai, as it can be found in lakes within the region. Peking duck and dim-sum are other popular dishes well know outside of China.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="rounded">
                <div>
                <p style="text-align: center;font-size: 40px;padding-top: 100px;color: white"><b><u>Sport</u></b></p>
                <div style="background-color: black;margin: 50px;width: 95%;height: 4000px;padding: 50px">
                    <div style=";float: left;;width: 48%">
                        <p style="color: white;text-align: center;font-size: 40px"><b><u>Table Tennis</u></b></p>
                        <div style="background-color: #e8e8e8;padding: 20px">
                            <div style="text-align: center">
                                <img src="https://img.freepik.com/free-vector/realistic-design-table-tennis-background_52683-46121.jpg?size=626&ext=jpg" alt="Lamp" width="400" height="300">
                            </div>
                            <p style="font-size: 20px"><b><u>1) China Olympic History</u></b></p>
                            <div class="text" style="padding-left: 30px">
                                <p>Table tennis, also known as ping pong, has been considered as the national sport of the People’s Republic of China.</p>
                                <p>Since table tennis became an official Olympic medal sport at Seoul 1988, Chinese athletes have dominated the sport winning 28 of a possible 32 gold medals up to Rio 2016. Since 1996, China has only missed out on gold once - in the men's singles at Athens 2004.</p>
                                <p>At Beijing 2008, new team events were introduced that replaced the men's and women's doubles. But the golden momentum of China's table tennis team continued. They won all of the gold medals at Beijing 2008, London 2012 and Rio 2016, just as they had done at Atlanta 1996 and Sydney 2000.</p>
                            </div>
                            <p style="font-size: 20px"><b><u>2) Athlete</u></b></p>
                            <div class="text" style="padding-left: 30px;padding-bottom: 50px">
                                <div style="text-align: center">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Ma_Long_2013.jpg/651px-Ma_Long_2013.jpg" alt="Lamp" width="300" height="300">
                                    <p><b>Ma Long</b></p>
                                </div>
                                <div style="text-align: Center">
                                    <table>
                                        <tr>
                                            <th>Born</th>
                                            <td>20 October 1988 (age 32)<br/>Anshan, Liaoning, China</td>
                                        </tr>
                                        <tr>
                                            <th>Playing Style</th>
                                            <td>Right-handed, shakehand grip</td>
                                        </tr>
                                        <tr>
                                            <th>Highest Ranking</th>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <th>Current Ranking</th>
                                            <td>3 (March 2021)</td>
                                        </tr>
                                        <tr>
                                            <th>Height</th>
                                            <td>1.75 m (5 ft 9 in)</td>
                                        </tr>
                                        <tr>
                                            <th>Weight</th>
                                            <td>72 kg (159 lb)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr class="rounded" style="border-top: 10px solid white">
                            <div class="text" style="padding-left: 30px;padding-top: 100px;padding-bottom: 50px">
                                <div style="text-align: center">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/ITTF_World_Tour_2017_German_Open_Xu_Xin_04.jpg/220px-ITTF_World_Tour_2017_German_Open_Xu_Xin_04.jpg" alt="Lamp" width="300" height="300">
                                    <p><b>Xu Xin</b></p>
                                </div>
                                <div style="text-align: Center">
                                    <table>
                                        <tr>
                                            <th>Born</th>
                                            <td>8 January 1990 (age 31)<br/>Xuzhou, Jiangsu, China</td>
                                        </tr>
                                        <tr>
                                            <th>Playing Style</th>
                                            <td>left-handed, penhold grip</td>
                                        </tr>
                                        <tr>
                                            <th>Highest Ranking</th>
                                            <td>1 (December 2019)</td>
                                        </tr>
                                        <tr>
                                            <th>Current Ranking</th>
                                            <td>2 (April 2021)</td>
                                        </tr>
                                        <tr>
                                            <th>Height</th>
                                            <td>1.81 m (5 ft 11 in)</td>
                                        </tr>
                                        <tr>
                                            <th>Weight</th>
                                            <td>75 kg (165 lb)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr class="rounded" style="border-top: 10px solid white">
                            <div class="text" style="padding-left: 30px;padding-top: 100px;padding-bottom: 50px">
                                <div style="text-align: center">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Mondial_Ping_-_Men%27s_Singles_-_Final_-_Zhang_Jike_vs_Wang_Hao_-_40.jpg/220px-Mondial_Ping_-_Men%27s_Singles_-_Final_-_Zhang_Jike_vs_Wang_Hao_-_40.jpg" alt="Lamp" width="300" height="300">
                                    <p><b>Zhang Jike</b></p>
                                </div>
                                <div style="text-align: Center">
                                    <table>
                                        <tr>
                                            <th>Born</th>
                                            <td>February 16, 1988 (age 33)<br/>Qingdao, Shandong, China</td>
                                        </tr>
                                        <tr>
                                            <th>Playing Style</th>
                                            <td>Right-handed, shakehand grip</td>
                                        </tr>
                                        <tr>
                                            <th>Highest Ranking</th>
                                            <td>1 (June to December 2012)</td>
                                        </tr>
                                        <tr>
                                            <th>Current Ranking</th>
                                            <td>75 (March 2019)</td>
                                        </tr>
                                        <tr>
                                            <th>Height</th>
                                            <td>1.78 m (5 ft 10 in)</td>
                                        </tr>
                                        <tr>
                                            <th>Weight</th>
                                            <td>73 kg (161 lb)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style=";float: right;width: 48%">
                        <p style="color: white;text-align: center;font-size: 40px"><b><u>Badminton</u></b></p>
                        <div style="background-color: #e8e8e8;padding: 20px;padding-bottom: 110px">
                            <div style="text-align: center">
                                <img src="https://wallpapercave.com/wp/wp1852922.jpg" alt="Lamp" width="400" height="300">
                            </div>
                            <p style="font-size: 20px"><b><u>1) China Olympic History</u></b></p>
                            <div class="text" style="padding-left: 30px">
                                <p>For the longest time, China has produced world-class shuttlers who have dominated the badminton circuit and established a neat Chinese empire. With legends like Lin Dan, Yang Yang, Zhao Jianhua and Han Jian, among others, China has had a long list of shuttlers beelining for success and cementing the nation as one of the most successful in badminton history.</p>
                                <p>There were several question marks as to who would be the rightful successor and keeper of this tradition of dominance when 5-time World Champion and 2-time Olympic gold medalist, Lin Dan hung up his racquet in 2020. Despite Super Dan not being around, China already has a trio of extremely talented shuttlers to its credit, ensuring their run at the upcoming Tokyo Olympics 2021 is medal-worthy.</p>
                            </div>
                            <p style="font-size: 20px"><b><u>2) Athlete</u></b></p>
                            <p style="font-size: 20px;padding-left: 50px"><b><u>2.1) Men's single</u></b></p>
                            <div class="text" style="padding-left: 30px;padding-bottom: 50px">
                                <div style="text-align: center">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/9/92/Chen_Long_%28London_2012%29.jpg" alt="Lamp" width="300" height="300">
                                    <p><b>Chen Long</b></p>
                                </div>
                                <div style="text-align: Center">
                                    <table>
                                        <tr>
                                            <th>Born</th>
                                            <td>18 January 1989 (age 32)<br/>Shashi District, Jingzhou, Hubei, China</td>
                                        </tr>
                                        <tr>
                                            <th>Highest Ranking</th>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <th>Current Ranking</th>
                                            <td>6 (20 April 2021)</td>
                                        </tr>
                                        <tr>
                                            <th>Height</th>
                                            <td>1.87 m (6 ft 2 in)</td>
                                        </tr>
                                        <tr>
                                            <th>Weight</th>
                                            <td>75 kg (165 lb)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr class="rounded" style="border-top: 10px solid white">
                            <p style="font-size: 20px;padding-left: 50px;padding-top: 100px"><b><u>2.1) Men's double</u></b></p>
                            <div class="text" style="padding-left: 30px;padding-bottom: 50px">
                                <div style="text-align: center">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Zhang_Nan.jpg/250px-Zhang_Nan.jpg" alt="Lamp" width="300" height="300">
                                    <p><b>Zhang Nan</b></p>
                                </div>
                                <div style="text-align: Center">
                                    <table>
                                        <tr>
                                            <th>Born</th>
                                            <td>1 March 1990 (age 31)<br/>Beijing, China</td>
                                        </tr>
                                        <tr>
                                            <th>Highest Ranking</th>
                                            <td>2 (MD 29 September 2016)<br/>1 (XD 27 January 2011)</td>
                                        </tr>
                                        <tr>
                                            <th>Current Ranking</th>
                                            <td>25 (MD), 421 (XD) (23 March 2021)</td>
                                        </tr>
                                        <tr>
                                            <th>Height</th>
                                            <td>1.83 m (6 ft 0 in)</td>
                                        </tr>
                                        <tr>
                                            <th>Weight</th>
                                            <td>74 kg (163 lb)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr class="rounded" style="border-top: 10px solid white">
                            <div class="text" style="padding-left: 30px;padding-top: 100px;padding-bottom: 50px">
                                <div style="text-align: center">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Fu_Haifeng%2C_Mens_Doubles_Badminton_Final_%288172656810%29.jpg/220px-Fu_Haifeng%2C_Mens_Doubles_Badminton_Final_%288172656810%29.jpg" alt="Lamp" width="300" height="300">
                                    <p><b>Fu Haifeng</b></p>
                                </div>
                                <div style="text-align: Center">
                                    <table>
                                        <tr>
                                            <th>Born</th>
                                            <td>23 August 1983 (age 37)<br/>Jieyang, Guangdong, China</td>
                                        </tr>
                                        <tr>
                                            <th>Highest Ranking</th>
                                            <td>1 (7 September 2006)</td>
                                        </tr>
                                        <tr>
                                            <th>Current Ranking</th>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <th>Height</th>
                                            <td>1.81 m (5 ft 11 in)/td>
                                        </tr>
                                        <tr>
                                            <th>Weight</th>
                                            <td>70 kg (154 lb; 11 st 0 lb)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
    </body>
</html>
