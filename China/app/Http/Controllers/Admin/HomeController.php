<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $admin = User::all()->where('is_admin','0')->where('admin_verified', '=', 0);
        $approve = User::all()->where('is_admin','0')->where('admin_verified', '=', 1);
        $reject = User::all()->where('is_admin','0')->where('admin_verified', '=', 2);
        return view('admin.home', compact('admin', 'approve', 'reject'));
//        return view('admin.home');
    }

    public function event()
    {
        return view('admin.adminevent');
    }


}
