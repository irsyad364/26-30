<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function Add(Request $request)
    {


        $event = new Event();
        $event->title =  $request->get('title');;
        $event->date =  $request->get('date');;
        $event->start_time =  $request->get('STime');
        $event->end_time =  $request->get('ETime');

        if($event->save())
        {
            return redirect('/admin/event')->with('message', trans('Success'));
        }
        else if (!$event->save())
        {
            return redirect('/admin/event')->with('message', trans('Fail'));
        }


    }
}
