<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\JoinEvent;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $user =  auth()-> user()->id;
        $users = User::all()->where('id',$user)->pluck('event');
        $event = Event::all()->wherein('event_id', $users);
        return view('user.home', compact('event'));
    }
    public function event()
    {
        $user =  auth()-> user()->id;
        $users = User::all()->where('id',$user);
//        $events = JoinEvent::pluck('event_id')->where('member_id', '=', $user);
        $user = User::all()->where('id',$user)->pluck('event');
        $event = Event::all()->whereNotIn('event_id', $user);
        return view('user.userevent', compact('event', 'users'));
    }

}
