<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApprovalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check())
        {
            if(!auth()-> user()->admin_verified== 1){
                if(auth()-> user()->is_admin== 1){
                    return $next($request);
                }
                else{
                    auth()->logout();
                    return redirect()-> route('login')->with('message', trans('Please wait for your account to be approved by Admin'));
                }

            }
            else if (auth()-> user()->admin_verified== 2){
                auth()->logout();

                return redirect()-> route('login')->with('message', trans('Your account has been rejected by Admin'));
            }


        }

        return $next($request);
    }
}
